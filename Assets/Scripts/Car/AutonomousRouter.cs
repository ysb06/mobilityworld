using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GraphRouter.Routers;
using GraphRouter;
using GraphRouter.Edges;
using QuikGraph;
using QuikGraph.Algorithms.Observers;
using QuikGraph.Algorithms.ShortestPath;

public class AutonomousRouter : BasicRouter
{
    public float ArrivalDetectionRange = 1.2f;
    private BidirectionalGraph<Node, Edge> routeGraph = new BidirectionalGraph<Node, Edge>(true);

    private void Start()
    {
        // 그래프 초기화
        EdgeData[] edgeData = FindObjectsOfType<EdgeData>();
        Edge[] edges = new Edge[edgeData.Length];
        for (int i = 0; i < edges.Length; i++)
        {
            edges[i] = edgeData[i].Edge;
        }
        routeGraph = new BidirectionalGraph<Node, Edge>(true);
        routeGraph.AddVerticesAndEdgeRange(edges);
        CurrentTarget = transform.position;
    }

    public IEnumerable<Edge> SetDestination(Node departure, Node destination)
    {
        //print($"From {departure} to {destination}");
        Func<Edge, double> costFunc = e => e.Distance;
        DijkstraShortestPathAlgorithm<Node, Edge> algorithm = new DijkstraShortestPathAlgorithm<Node, Edge>(routeGraph, costFunc);
        VertexPredecessorRecorderObserver<Node, Edge> observer = new VertexPredecessorRecorderObserver<Node, Edge>();
        using (observer.Attach(algorithm))
        {
            algorithm.Compute(departure);
            if (observer.TryGetPath(destination, out IEnumerable<Edge> path))
            {
                /*foreach(Edge edge in path)
                {
                    print($"{edge.Source} ==> {edge.Target}");
                }*/
                SetRoute(path);
                Initialize();
                enabled = true;
                return path;
            }
            else
            {
                if (departure == destination)
                {
                    Edge[] route = new Edge[] { CurrentEdge };

                    return route;
                }
                else
                {
                    Debug.LogWarning("Unknown Error");
                    return null;
                }
            }
        }
    }

    protected override bool NotifiyArrived(Vector3 current, Vector3 target)
    {
        transform.position = Vector3.MoveTowards(transform.position, target + new Vector3(0, 0.0001f, 0), Speed * Time.deltaTime);

        return Vector3.Magnitude(current - target) < ArrivalDetectionRange;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        foreach (Edge line in CurrentRoute)
        {
            Gizmos.DrawLine(line.Source.Position, line.Target.Position);
        }
    }
}
