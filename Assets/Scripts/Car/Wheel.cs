using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel : MonoBehaviour
{
    public DrivingSystem Car;
    public bool Reversed = false;

    private void Start()
    {
        Car = GetComponentInParent<DrivingSystem>();
    }

    
    private void Update()
    {
        if (Reversed)
        {
            transform.Rotate(0, 0, -Car.CurrentSpeed / 3);
        }
        else
        {
            transform.Rotate(0, 0, Car.CurrentSpeed / 3);
        }
    }
}
