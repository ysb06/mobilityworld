using GraphRouter;
using UnityEngine;
using UnityEngine.SceneManagement;
using GraphRouter.Edges;
using System.Collections.Generic;

[RequireComponent(typeof(AutonomousRouter))]
public class DrivingSystem : MonoBehaviour
{
    private const float SPEED_CORRECTION = 10f;

    private AutonomousRouter router = null;
    private ADASystem ADAS;
    private CarBody bodyObject;
    private MusicPlayer musicPlayer;

    public Node[] Destinations;

    private Vector3 prevPosition = Vector3.zero;
    private Queue<float> speedQueue = new Queue<float>(12);

    public float CurrentSpeed
    {
        //get { return router.Speed * 3.6f; }
        get {
            float sum = 0;
            foreach(float v in speedQueue)
            {
                sum += v;
            }
            if (speedQueue.Count == 0)
                return 0;
            else
                return sum / speedQueue.Count * SPEED_CORRECTION; 
        }
        set
        {
            ADAS.DestSpeed = value;
        }
    }

    public Node CurrentNode
    {
        get
        {
            if (router.IsArrived == false)
            {
                return router.CurrentEdge.Target;
            }
            else if (Destinations.Length > 0)
                return Destinations[0];
            else
            {
                Debug.LogWarning("There is no destinations for this car");
                return null;
            }
        }
    }
    public bool BodyVisible
    {
        get { return bodyObject.gameObject.activeSelf; }
        set { bodyObject.gameObject.SetActive(value); }
    }
    public float HeadUnitVolume = 0.5f;
    public float BatteryChargingRate = 0.5f;
    public float DrivingTime = 0f;
    public float CurrentRouteDistance = 0;
    public float MovedDistance = 0;
    public float Temperature = 24;
    public float SpeedInt = 0;

    private void Awake()
    {
        /*if (FindObjectsOfType<DrivingSystem>().Length <= 1)
        {
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }*/
    }

    private void Start()
    {
        bodyObject = GetComponent<CarBody>();
        router = GetComponent<AutonomousRouter>();
        if (router == null)
        {
            Debug.LogError("There is no router for this car.");
        }
        ADAS = GetComponentInChildren<ADASystem>();

        if (Destinations != null && Destinations.Length > 0)
        {
            transform.position = Destinations[0].Position + new Vector3(0, 1, 0);
            transform.rotation = Destinations[0].transform.rotation;
        }
        else
        {
            Debug.LogError("This car not connected to graph");
        }
        musicPlayer = GetComponentInChildren<MusicPlayer>();
    }

    public void ChangeDestination(int destination)
    {
        var route = router.SetDestination(CurrentNode, Destinations[destination]);
        CurrentRouteDistance = 0f;
        foreach (Edge edge in route)
        {
            CurrentRouteDistance += edge.Distance;
        }
        MovedDistance = 0;
    }

    public void ChangeMusic(int i)
    {
        musicPlayer.PlayMusic(i);
    }

    public void ChangeVideo()
    {

    }

    private void Update()
    {
        if (router.enabled)
        {
            DrivingTime += Time.deltaTime;
        }

        float moveDistance = Vector3.Magnitude(transform.position - prevPosition);
        speedQueue.Enqueue(moveDistance / Time.deltaTime);
        if (speedQueue.Count > 12)
        {
            speedQueue.Dequeue();
        }
        MovedDistance += moveDistance;
        prevPosition = transform.position;

        int Speed = Mathf.RoundToInt(CurrentSpeed);
        if (Speed > SpeedInt)
        {
            SpeedInt += 1;
        }
        else if (Speed < SpeedInt)
        {
            SpeedInt -= 1;
        }
        else
        {
            SpeedInt = Speed;
        }

        BatteryChargingRate -= 0.0000001f;
        Temperature += Random.Range(-0.01f, 0.01f);
    }
}
