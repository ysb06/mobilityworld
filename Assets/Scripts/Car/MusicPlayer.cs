using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    public DrivingSystem PlayerCar;

    public AudioSource[] MusicList;
    public AudioSource CurrentMusic;

    private void Start()
    {
        if (PlayerCar == null)
        {
            PlayerCar = GetComponentInParent<DrivingSystem>();
        }
    }

    public void PlayMusic(int target)
    {
        CurrentMusic = MusicList[target];
        CurrentMusic.Play();
    }

    public void PlayMusic()
    {
        CurrentMusic = MusicList[0];
        CurrentMusic.Play();
    }

    public void StopMusic()
    {
        CurrentMusic.Stop();
    }

    private void Update()
    {
        if (CurrentMusic != null)
        {
            CurrentMusic.volume = PlayerCar.HeadUnitVolume;
        }
    }
}
