using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DrivingSystem))]
public class DrivingSystemEditor : Editor
{
    private DrivingSystem origin;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        origin = (DrivingSystem)target;

        if (GUILayout.Button("Move to 0"))
        {
            origin.ChangeDestination(0);
        }
        if (GUILayout.Button("Move to 1"))
        {
            origin.ChangeDestination(1);
        }
        if (GUILayout.Button("Move to 2"))
        {
            origin.ChangeDestination(2);
        }
        if (GUILayout.Button("Move to 3"))
        {
            origin.ChangeDestination(3);
        }
    }
}
