using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ADASystem : MonoBehaviour
{
    private AutonomousRouter router;
    public List<GameObject> obstacles = new List<GameObject>();
    public float DetectionRange = 0f;
    public float BrakeRate = 0.5f;
    public float DestSpeed = 5f;

    private void Start()
    {
        router = GetComponentInParent<AutonomousRouter>();
        DetectionRange = GetComponent<BoxCollider>().size.z / 2;
    }

    private void Update()
    {
        if (obstacles.Count > 0)
        {
            List<GameObject> nonTargets = new List<GameObject>();
            foreach(GameObject obj in obstacles)
            {
                try
                {
                    if (Vector3.Magnitude(obj.transform.position - transform.position) <= DetectionRange)
                    {
                        router.Speed -= BrakeRate;
                        if (router.Speed < 0)
                        {
                            router.Speed = 0;
                        }
                        break;
                    }
                    else
                    {
                        nonTargets.Add(obj);
                    }
                }
                catch (MissingReferenceException)
                {
                    nonTargets.Add(obj);
                }
            }
            foreach(GameObject obj in nonTargets)
            {
                obstacles.Remove(obj);
            }
        }
        else
        {
            if (router.Speed < DestSpeed)
            {
                router.Speed += BrakeRate;
                if (router.Speed > DestSpeed)
                {
                    router.Speed = DestSpeed;
                }
            }
            else if (router.Speed > DestSpeed)
            {
                router.Speed -= BrakeRate;
                if (router.Speed < DestSpeed)
                {
                    router.Speed = DestSpeed;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponentInParent<FCG.TrafficCar>() != null)
        {
            obstacles.Add(other.gameObject);
        }
    }
}
