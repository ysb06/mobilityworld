using System.Collections.Generic;
using UnityEngine;
using QuikGraph;
using System.Collections;
using System;

namespace GraphRouter.Edges
{
    [Serializable]
    public class Edge : IEdge<Node>, IEnumerable<Vector3>, IDisposable
    {
        [field: SerializeField]
        public Node Source { get; protected set; }
        [field: SerializeField]
        public Node Target { get; protected set; }

        public float Distance
        {
            get
            {
                return Vector3.Distance(Source.Position, Target.Position);
            }
        }
        [SerializeField]
        protected Vector3[] Checkpoints;

        public virtual int PointCount { get { return Checkpoints.Length + 2; } }

        public Edge(Node origin, Node target, Vector3[] checkpoints)
        {
            Source = origin;
            Source.OutEdges.Add(this);
            Target = target;
            Target.InEdges.Add(this);

            Checkpoints = checkpoints;
        }

        public Edge(Node origin, Node target) : this(origin, target, new Vector3[0]) { }

        public virtual Vector3 this[int index]
        {
            get {
                if (index == 0)
                {
                    return Source.Position;
                }
                else if (index == Checkpoints.Length + 1)
                {
                    return Target.Position;
                }
                else
                {
                    return Checkpoints[index - 1];
                } 
            }
        }

        public void Dispose()
        {
            Source.OutEdges.Remove(this);
            Target.InEdges.Remove(this);
        }

        public IEnumerator<Vector3> GetEnumerator()
        {
            yield return Source.Position;
            foreach (Vector3 var in Checkpoints)
            {
                yield return var;
            }
            yield return Target.Position;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            return $"{Source.Position}--{Target.Position}";
        }
    }
}