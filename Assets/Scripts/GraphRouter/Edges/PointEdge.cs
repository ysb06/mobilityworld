﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GraphRouter.Edges
{
    public class PointEdge : Edge
    {
        public PointEdge(Node origin, Node target) : base(origin, target) { }

        public PointEdge(Node origin, Node target, Vector3[] checkpoints) : base(origin, target, checkpoints) { }
    }
}
