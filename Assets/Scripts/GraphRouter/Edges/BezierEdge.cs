﻿using UnityEngine;

namespace GraphRouter.Edges
{
    public class BezierEdge : Edge
    {
        private Vector3 sourceControlPoint;
        private Vector3 targetControlPoint;
        private float resolution = 0.01f;

        public Vector3 SourceControlPoint
        {
            get => sourceControlPoint;
            set
            {
                sourceControlPoint = value;
                Initialize();
            }
        }

        public Vector3 TargetControlPoint
        {
            get => targetControlPoint;
            set
            {
                targetControlPoint = value;
                Initialize();
            }
        }

        public float Resolution
        {
            get => resolution;
            set
            {
                resolution = value;
                Initialize();
            }
        }

        public BezierEdge(Node origin, Node target, Vector3 controlOrigin, Vector3 controlTarget, float resolution)
            : base(origin, target, new Vector3[Mathf.CeilToInt(1 / resolution) - 1])
        {
            sourceControlPoint = controlOrigin;
            targetControlPoint = controlTarget;
            this.resolution = resolution;

            Initialize();
        }

        private void Initialize()
        {
            for (int i = 0; i < Checkpoints.Length; i++)
            {
                float t = i * resolution;
                Checkpoints[i] =
                    (1 - t) * (1 - t) * (1 - t) * Source.Position
                    + 3 * (1 - t) * (1 - t) * t * sourceControlPoint
                    + 3 * (1 - t) * t * t * targetControlPoint
                    + t * t * t * Target.Position;
            }
        }
    }
}
