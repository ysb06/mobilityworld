using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GraphRouter.Routers
{
    public class RigidRouter : Router
    {
        public Rigidbody TargetBody = null;
        public float Acceleration = 1f;
        public float Allowance = 0.1f;
        public bool IgnoreHeight = true;
        private Vector3 direction = new Vector3();

        public Vector3 AccelerationVector
        {
            get
            {
                return Acceleration * direction;
            }
        }

        private void Start()
        {
            TargetBody ??= GetComponent<Rigidbody>();
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override bool NotifiyArrived(Vector3 current, Vector3 target)
        {
            if (IgnoreHeight)
            {
                target = new Vector3(target.x, current.y, target.z);
            }
            float distance = Vector3.Distance(target, current);
            direction = Vector3.Normalize(target - current);

            TargetBody.AddForce(AccelerationVector);

            return distance < Allowance;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawSphere(CurrentTarget, 0.11f);

            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.position, transform.position + AccelerationVector);
        }
    }
}