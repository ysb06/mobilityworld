using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GraphRouter;
using GraphRouter.Routers;
using GraphRouter.Edges;

public class TestRouteCreator : MonoBehaviour
{
    public BasicRouter target;
    public List<Node> NodeList = new List<Node>();

    private List<Edge> edgeList = new List<Edge>();

    public Vector3 control1 = new Vector3();
    public Vector3 control2 = new Vector3();
    public float resolution = 0.01f;

    private void Start()
    {
        Node prev = null;
        foreach (Node node in NodeList)
        {
            if (prev != null)
            {
                edgeList.Add(new PointEdge(prev, node));
            }
            prev = node;
        }
        edgeList.Add(
            new BezierEdge(
                NodeList[NodeList.Count - 1], NodeList[0],
                control1, control2,
                resolution
            )
        );

        if (target != null)
        {
            target.SetRoute(edgeList);
            target.Initialize();
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(NodeList[0].Position, control2);
        Gizmos.DrawLine(NodeList[NodeList.Count - 1].Position, control1);
    }
}
