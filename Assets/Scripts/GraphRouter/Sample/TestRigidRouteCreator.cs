using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GraphRouter;
using GraphRouter.Routers;
using GraphRouter.Edges;

public class TestRigidRouteCreator : MonoBehaviour
{
    public Router target = null;
    public Node[] NodeList = new Node[6];

    private List<Edge> edgeList = new List<Edge>();

    private void Start()
    {
        Node prev = null;
        foreach (Node node in NodeList)
        {
            if (prev != null)
            {
                edgeList.Add(new PointEdge(prev, node));
            }
            prev = node;
        }
        edgeList.Add(
            new BezierEdge(
                NodeList[NodeList.Length - 1], NodeList[0],
                new Vector3(12.5f, 0, 2.5f), new Vector3(14.5f, 0, 2.5f),
                0.1f
            )
        );

        if (target != null)
        {
            target.SetRoute(edgeList);
            target.Initialize();
        }
    }
}
