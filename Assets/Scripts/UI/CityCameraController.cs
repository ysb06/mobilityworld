using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityCameraController : MonoBehaviour
{
    public Vector3 Offset = new Vector3(0, 2f, -1f);
    public bool IsFollowTargetRotation = true;

    private void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject obj in objs)
        {
            if (obj.name == "Player Car")
            {
                transform.parent = obj.transform;
                transform.localPosition = Offset;
                print("Player Car Found");
                return;
            }
        }
        Debug.LogWarning("Player Car not Found");
    }
}
