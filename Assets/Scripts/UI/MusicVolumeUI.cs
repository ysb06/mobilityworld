using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicVolumeUI : MonoBehaviour
{
    public DrivingSystem PlayerCar;
    public Slider VolumeSlider;

    private void Start()
    {
        if (PlayerCar == null)
        {
            PlayerCar = GetComponentInParent<DrivingSystem>();
        }

        if (VolumeSlider == null)
        {
            VolumeSlider = GetComponent<Slider>();
        }
    }

    private void Update()
    {
        PlayerCar.HeadUnitVolume = VolumeSlider.value;
    }
}
