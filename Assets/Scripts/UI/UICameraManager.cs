using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICameraManager : MonoBehaviour
{
    private void Start()
    {
        GameObject[] playerObj = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject obj in playerObj)
        {
            Canvas canvas = obj.GetComponentInChildren<Canvas>();
            if (canvas != null)
            {
                canvas.worldCamera = Camera.main;
            }
        }
    }
}
