using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BatteryUI : MonoBehaviour
{
    public DrivingSystem PlayerCar;
    public Image[] BatteryBlocks;
    public TextMeshProUGUI NumberText;

    private void Start()
    {
        if (PlayerCar == null)
        {
            PlayerCar = GetComponentInParent<DrivingSystem>();
        }
    }

    private void Update()
    {
        float battery = PlayerCar.BatteryChargingRate;
        float full = BatteryBlocks.Length;
        int current = Mathf.RoundToInt(battery * full);
        if (current > BatteryBlocks.Length)
        {
            current = BatteryBlocks.Length;
        }
        
        for (int i = 0; i < current; i++)
        {
            BatteryBlocks[i].enabled = true;
        }

        for (int i = current; i < BatteryBlocks.Length; i++)
        {
            BatteryBlocks[i].enabled = false;
        }

        NumberText.text = Mathf.RoundToInt(battery * 100).ToString();
    }
}
