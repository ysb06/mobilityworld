using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SpeedMeter : MonoBehaviour
{
    public DrivingSystem PlayerCar;
    public TextMeshProUGUI SpeedText;

    private void Start()
    {
        if (PlayerCar == null)
        {
            PlayerCar = GetComponentInParent<DrivingSystem>();
        }
    }

    public void Update()
    {
        SpeedText.text = PlayerCar.SpeedInt.ToString();
    }
}
