using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TemperatureUI : MonoBehaviour
{
    public DrivingSystem PlayerCar;
    public TextMeshProUGUI ValueText;

    private void Start()
    {
        if (PlayerCar == null)
        {
            PlayerCar = GetComponentInParent<DrivingSystem>();
        }
    }

    private void Update()
    {
        if (ValueText != null)
        {
            ValueText.text = Mathf.RoundToInt(PlayerCar.Temperature).ToString();
        }
    }
}
