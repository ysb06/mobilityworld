using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WayNodeGroup))]
public class WayNodeGroupEditor : Editor
{
    private WayNodeGroup orgin;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        orgin = (WayNodeGroup)target;

        if (GUILayout.Button("Refresh"))
        {
            NodeManager nodeParentManager = FindObjectOfType<NodeManager>();
            EdgeManager edgeParentManager = FindObjectOfType<EdgeManager>();
            GameObject edgeParent;
            GameObject nodeParent;
            if (nodeParentManager == null)
            {
                nodeParent = new GameObject("Node Groups");
                Debug.LogWarning("Graph looks not initialized");
            }
            else
            {
                nodeParent = nodeParentManager.gameObject;
            }
            if (edgeParentManager == null)
            {
                edgeParent = new GameObject("Node Groups");
                Debug.LogWarning("Graph looks not initialized");
            }
            else
            {
                edgeParent = edgeParentManager.gameObject;
            }
            orgin.Initialize(nodeParent, edgeParent);
        }
    }
}
