using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using FCG;

public class WayNodeConsole : EditorWindow
{
    [MenuItem("Window/WayNode Manager")]
    static void Open()
    {
        WayNodeConsole window = GetWindow<WayNodeConsole>();
        window.titleContent.text = "WayNode Manager";
    }

    void OnGUI()
    {
        EditorGUILayout.LabelField("전체 노드 초기화 및 생성");
        if (GUILayout.Button("Initialize All"))
        {
            InitializeAll();
        }
    }

    private void InitializeAll()
    {
        FCGWaypointsContainer[] targets = FindObjectsOfType<FCGWaypointsContainer>();

        NodeManager OldNodeList = FindObjectOfType<NodeManager>();
        EdgeManager OldEdgeList = FindObjectOfType<EdgeManager>();

        GameObject nodeParent = new GameObject("Node Groups");
        nodeParent.AddComponent<NodeManager>();
        GameObject edgeParent = new GameObject("Edge Groups");
        edgeParent.AddComponent<EdgeManager>();

        // 그룹 초기화
        foreach (FCGWaypointsContainer target in targets)
        {
            WayNodeGroup nodeGroup = target.gameObject.GetComponent<WayNodeGroup>();
            if (nodeGroup == null)
            {
                nodeGroup = target.gameObject.AddComponent<WayNodeGroup>();
            }
            nodeGroup.Initialize(nodeParent, edgeParent);
        }

        // 노드 그룹 간 연결
        foreach (FCGWaypointsContainer sourcePoint in targets)
        {
            WayNodeGroup source = sourcePoint.gameObject.GetComponent<WayNodeGroup>();
            Debug.Log($"---------------- {source.transform.parent.parent.name} / {source.name} ----------------");
            source.ConnectToOther(edgeParent);
        }

        if (OldNodeList != null)
            DestroyImmediate(OldNodeList.gameObject);
        if (OldEdgeList != null)
            DestroyImmediate(OldEdgeList.gameObject);
    }
}
