using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GraphRouter;
using GraphRouter.Edges;

public class EdgeData : MonoBehaviour
{
    [SerializeField]
    private Edge Core = null;
    public Node StartNode
    {
        get
        {
            if (Core == null)
            {
                return null;
            }
            else
            {
                return Core.Source;
            }
        }
    }
    public Node EndNode
    {
        get
        {
            if (Core == null)
            {
                return null;
            }
            else
            {
                return Core.Target;
            }
        }
    }
    public Edge Edge
    {
        get { return Core; }
    }

    public void Initialize(Node start, Node end)
    {
        if (Core != null)
        {
            Core.Dispose();
        }
        Core = new PointEdge(start, end);
    }

    private void OnDrawGizmos()
    {
        if (StartNode != null && EndNode != null)
        {
            Vector3 adj = (StartNode.Position - EndNode.Position).normalized / 10;

            Gizmos.color = Color.blue;
            Gizmos.DrawLine(StartNode.Position, EndNode.Position + adj * 3);
            Gizmos.DrawSphere(EndNode.Position + adj * 3, 0.1f);
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (StartNode != null && EndNode != null)
        {
            Vector3 adj = (StartNode.Position - EndNode.Position).normalized / 10;

            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(StartNode.Position, EndNode.Position + adj * 3);
            Gizmos.DrawSphere(EndNode.Position + adj * 3, 0.1f);
        }
    }

    private void OnDestroy()
    {
        Core.Dispose();
    }
}
