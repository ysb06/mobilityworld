using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FCG;
using GraphRouter;
using GraphRouter.Edges;
using System;

[Serializable]
public struct NodeList
{
    [HideInInspector]
    public string Name;
    [SerializeField]
    public List<Node> Nodes;

    public override string ToString()
    {
        return Name;
    }
}

[ExecuteInEditMode]
[RequireComponent(typeof(FCGWaypointsContainer))]
public class WayNodeGroup : MonoBehaviour
{
    public FCGWaypointsContainer nodeOrigin { get; private set; }
    public GameObject NodeListObject = null;
    public GameObject EdgeListObject = null;
    public List<NodeList> RightRoadLines = new List<NodeList>();
    public List<NodeList> LeftRoadLines = new List<NodeList>();
    public List<EdgeData> RoadEdges = new List<EdgeData>();
    public int Count = 2;

    private void Awake()
    {
        if (nodeOrigin == null)
            nodeOrigin = GetComponent<FCGWaypointsContainer>();
    }

    public void Initialize(GameObject nodeParent, GameObject edgeParent)
    {
        NodeListObject = nodeParent;
        EdgeListObject = edgeParent;

        // 현재 간선 삭제
        foreach (EdgeData edge in RoadEdges)
        {
            DestroyImmediate(edge.gameObject);
        }
        RoadEdges.Clear();

        // 현재 노드 목록 삭제
        foreach (NodeList road in RightRoadLines)
        {
            foreach(Node node in road.Nodes)
            {
                DestroyImmediate(node.gameObject);
            }
            road.Nodes.Clear();
        }
        RightRoadLines.Clear();

        foreach (NodeList road in LeftRoadLines)
        {
            foreach (Node node in road.Nodes)
            {
                DestroyImmediate(node.gameObject);
            }
            road.Nodes.Clear();
        }
        LeftRoadLines.Clear();

        // Count에 따른 도로 수 초기화
        for (int i = 0; i < Count; i++)
        {
            RightRoadLines.Add(new NodeList() { Nodes = new List<Node>(), Name = $"Line {i}" }); ;
            if (nodeOrigin.oneway && i == 0)
            {
                continue;
            }
            else
            {
                LeftRoadLines.Add(new NodeList() { Nodes = new List<Node>(), Name = $"Line {i}" }); ;
            }
        }
        /*print($"{RightRoadLines.Count}, {LeftRoadLines.Count}");*/

        // 노드 생성 또는 재생성
        foreach (Transform waypoint in nodeOrigin.waypoints)
        {
            GameObject nodeGroup = new GameObject($"{waypoint.name} Group");
            nodeGroup.transform.parent = nodeParent.transform;
            nodeGroup.transform.position = waypoint.position;
            nodeGroup.transform.rotation = waypoint.rotation;

            Vector3 offset = nodeGroup.transform.right * nodeOrigin.width;

            if (nodeOrigin.oneway)
            {
                for (int i = 0; i < Count; i++)
                {
                    if (i == 0)
                    {
                        GameObject nodeC = new GameObject($"{nodeGroup.name} C{i}");
                        RightRoadLines[i].Nodes.Add(nodeC.AddComponent<Node>());
                        nodeC.transform.parent = nodeGroup.transform;
                        nodeC.transform.position = nodeGroup.transform.position;
                        nodeC.transform.rotation = nodeGroup.transform.rotation;
                        continue;
                    }

                    GameObject nodeR = new GameObject($"{nodeGroup.name} R{i}");
                    RightRoadLines[i].Nodes.Add(nodeR.AddComponent<Node>());
                    nodeR.transform.parent = nodeGroup.transform;
                    nodeR.transform.position = nodeGroup.transform.position + (2 * i) * offset;
                    nodeR.transform.rotation = nodeGroup.transform.rotation;


                    GameObject nodeL = new GameObject($"{nodeGroup.name} L{i}");
                    LeftRoadLines[i - 1].Nodes.Add(nodeL.AddComponent<Node>());
                    nodeL.transform.parent = nodeGroup.transform;
                    nodeL.transform.position = nodeGroup.transform.position - (2 * i) * offset;
                    nodeL.transform.rotation = nodeGroup.transform.rotation;
                }
            }
            else
            {
                for (int i = 0; i < Count; i++)
                {
                    GameObject nodeR = new GameObject($"{nodeGroup.name} R{i}");
                    RightRoadLines[i].Nodes.Add(nodeR.AddComponent<Node>());
                    nodeR.transform.parent = nodeGroup.transform;
                    nodeR.transform.position = nodeGroup.transform.position + (2 * i + 1) * offset;
                    nodeR.transform.rotation = nodeGroup.transform.rotation;


                    GameObject nodeL = new GameObject($"{nodeGroup.name} L{i}");
                    LeftRoadLines[i].Nodes.Add(nodeL.AddComponent<Node>());
                    nodeL.transform.parent = nodeGroup.transform;
                    nodeL.transform.position = nodeGroup.transform.position - (2 * i + 1) * offset;
                    nodeL.transform.rotation = nodeGroup.transform.rotation;
                }
            }
        }
        if (nodeOrigin.oneway)
        {
            RightRoadLines.AddRange(LeftRoadLines);
            LeftRoadLines.Clear();
        }

        // 노드 그룹 내 간선 연결
        foreach (NodeList nodeList in RightRoadLines)
        {
            Node prevNode = null;
            foreach (Node node in nodeList.Nodes)
            {
                if (prevNode == null)
                {
                    prevNode = node;
                    continue;
                }
                else
                {
                    GameObject edgeObj = new GameObject($"{prevNode.name} -- {node.name}");
                    EdgeData edge = edgeObj.AddComponent<EdgeData>();
                    edge.Initialize(prevNode, node);
                    RoadEdges.Add(edge);
                    edge.transform.parent = edgeParent.transform;

                    prevNode = node;
                }
            }
        }

        foreach (NodeList nodeList in LeftRoadLines)
        {
            Node prevNode = null;
            nodeList.Nodes.Reverse();
            foreach (Node node in nodeList.Nodes)
            {
                if (prevNode == null)
                {
                    prevNode = node;
                    continue;
                }
                else
                {
                    GameObject edgeObj = new GameObject($"{prevNode.name} -- {node.name}");
                    EdgeData edge = edgeObj.AddComponent<EdgeData>();
                    edge.transform.parent = edgeParent.transform;
                    edge.Initialize(prevNode, node);
                    RoadEdges.Add(edge);
                    

                    prevNode = node;
                }
            }
        }

        print($"Initialized {name}");
    }

    public void ConnectToOther(GameObject edgeParent)
    {
        Debug.Log("--- Way 0");
        for (int i = 0; i < nodeOrigin.nextWay0.Length; i++)
        {
            WayNodeGroup other = nodeOrigin.nextWay0[i].GetComponent<WayNodeGroup>();
            int side = nodeOrigin.nextWaySide0[i];
            Debug.Log($"{other.transform.parent.parent.name} / {other.name} ({side})");

            foreach (NodeList line in LeftRoadLines)
            {
                Node source = line.Nodes[line.Nodes.Count - 1];

                if (side == 0)
                {
                    foreach (NodeList targetLine in other.LeftRoadLines)
                    {
                        Node target = targetLine.Nodes[0];

                        GameObject edgeObj = new GameObject($"{source.name} -- {target.name}");
                        EdgeData edge = edgeObj.AddComponent<EdgeData>();
                        edge.transform.parent = edgeParent.transform;
                        edge.Initialize(source, target);
                        RoadEdges.Add(edge);
                        if (side != 0)
                            Debug.Log(edgeObj.name);
                    }
                }
                else
                {
                    foreach (NodeList targetLine in other.RightRoadLines)
                    {
                        Node target = targetLine.Nodes[0];

                        GameObject edgeObj = new GameObject($"{source.name} -- {target.name}");
                        EdgeData edge = edgeObj.AddComponent<EdgeData>();
                        edge.transform.parent = edgeParent.transform;
                        edge.Initialize(source, target);
                        RoadEdges.Add(edge);
                        if (side != 0)
                            Debug.Log(edgeObj.name);
                    }
                }
            }
        }

        Debug.Log("--- Way 1");
        for (int i = 0; i < nodeOrigin.nextWay1.Length; i++)
        {
            WayNodeGroup other = nodeOrigin.nextWay1[i].GetComponent<WayNodeGroup>();
            int side = nodeOrigin.nextWaySide1[i];
            Debug.Log($"{other.transform.parent.parent.name} / {other.name} ({side})");

            foreach (NodeList line in RightRoadLines)
            {
                Node source = line.Nodes[line.Nodes.Count - 1];

                if (side == 0)
                {
                    foreach (NodeList targetLine in other.LeftRoadLines)
                    {
                        Node target = targetLine.Nodes[0];

                        GameObject edgeObj = new GameObject($"{source.name} -- {target.name}");
                        EdgeData edge = edgeObj.AddComponent<EdgeData>();
                        edge.transform.parent = edgeParent.transform;
                        edge.Initialize(source, target);
                        RoadEdges.Add(edge);
                        Debug.Log(edgeObj.name);
                    }
                }
                else
                {
                    foreach (NodeList targetLine in other.RightRoadLines)
                    {
                        Node target = targetLine.Nodes[0];

                        GameObject edgeObj = new GameObject($"{source.name} -- {target.name}");
                        EdgeData edge = edgeObj.AddComponent<EdgeData>();
                        edge.transform.parent = edgeParent.transform;
                        edge.Initialize(source, target);
                        RoadEdges.Add(edge);
                        Debug.Log(edgeObj.name);
                    }
                }
            }
        }
    }

    private void OnDrawGizmos()
    {

    }
}
