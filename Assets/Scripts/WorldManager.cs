using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class WorldManager : MonoBehaviour
{
    public static bool SCENE_CHANGER = true;

    private bool isMetaverse = false;

    public GameObject[] VehicleSystemGroup;
    public Rigidbody PlayerCarBody;
    public Canvas PlayerCarUI;
    //public GameObject[] CityGroup;
    //public GameObject[] MetaverseGroup;

    private void Awake()
    {
        if (FindObjectsOfType<WorldManager>().Length <= 1)
        {
            DontDestroyOnLoad(this);
            if (VehicleSystemGroup != null)
                foreach (GameObject obj in VehicleSystemGroup)
                {
                    DontDestroyOnLoad(obj);
                }
        }
        else
        {
            Destroy(this);
            if (VehicleSystemGroup != null)
                foreach (GameObject obj in VehicleSystemGroup)
                {
                    Destroy(obj);
                }
        }
    }

    public void SwitchMetaverse()
    {
        if (isMetaverse)
        {
            PlayerCarBody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            isMetaverse = false;
            SceneManager.LoadScene(0);
        }
        else
        {
            PlayerCarBody.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
            Destroy(PlayerCarBody.GetComponentInChildren<Camera>().gameObject);
            isMetaverse = true;
            SceneManager.LoadScene(1);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SwitchMetaverse();
        }
    }
}
